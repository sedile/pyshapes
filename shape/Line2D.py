from shape.Shape2D import Shape2D
from shape.Point2D import Point2D
from math import sqrt, pow

class Line2D(Shape2D):
    
    def __init__(self, x1, y1, x2, y2):
        Shape2D.__init__(self)
        self._points = (Point2D(x1,y1),Point2D(x2,y2))
        
    @classmethod
    def createLine2DFromPoint2D(cls, a, b):
        return Line2D(a.getX(), a.getY(), b.getX(), b.getY())

    def getStartPoint(self):
        return self._points[0]
    
    def getEndPoint(self):
        return self._points[1]

    def getShapeType(self):
        return "Line2D"
    
    def computePerimeter(self):
        d1 = pow((self._points[1].getX() - self._points[0].getX()), 2)
        d2 = pow((self._points[1].getY() - self._points[1].getY()), 2)
        dist = sqrt(d1 + d2)
        return dist
    
    def __eq__(self, other):
        return self._points == other._points
    
    def __hash__(self):
        return hash(self._points)
    
    def __repr__(self):
        return '(' + self.getShapeType() + repr(self._points[0]) + repr(self._points[1]) + ')'