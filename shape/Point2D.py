from shape.Shape2D import Shape2D

class Point2D(Shape2D):
    
    def __init__(self, x, y):
        Shape2D.__init__(self)
        self._x = x
        self._y = y
        
    def getX(self):
        return self._x
    
    def getY(self):
        return self._y
    
    def getShapeType(self):
        return "Point2D"
    
    def computePerimeter(self):
        return 1.0
    
    def __eq__(self, other):
        return self._x == other._x and self._y == other._y
    
    def __hash__(self):
        return hash((self._x, self._y))
    
    def __repr__(self):
        return '(' + self.getShapeType() + '( ' + str(self._x) + ' ' + str(self._y) + ' ))'