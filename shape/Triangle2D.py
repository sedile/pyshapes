from shape.Shape2D import Shape2D
from shape.Point2D import Point2D
from shape.Line2D import Line2D

class Triangle2D(Shape2D):
    
    def __init__(self, x1, y1, x2, y2, x3, y3):
        Shape2D.__init__(self)
        self._points = (Point2D(x1,y1),Point2D(x2,y2),Point2D(x3,y3))
        
    @classmethod
    def createTriangle2DFromPoint2D(cls, a, b, c):
        return Triangle2D(a.getX(), a.getY(), b.getX(), b.getY(), c.getX(), c.getY())
        
    def getLowerLeft(self):
        return self._points[0]
    
    def getLowerRight(self):
        return self._points[1]
    
    def getTop(self):
        return self._points[2]

    def getShapeType(self):
        return "Triangle2D"
    
    def computePerimeter(self):
        la = Line2D.createLine2DFromPoint2D(self._points[0],self._points[1])
        lb = Line2D.createLine2DFromPoint2D(self._points[1],self._points[2])
        lc = Line2D.createLine2DFromPoint2D(self._points[2],self._points[0])
        perimeter = la.computePerimeter() + lb.computePerimeter() + lc.computePerimeter()
        return perimeter
    
    def __eq__(self, other):
        return self._points == other._points
    
    def __hash__(self):
        return hash(self._points)
    
    def __repr__(self):
        return '(' + self.getShapeType() + repr(self._points[0]) + repr(self._points[1]) + repr(self._points[2]) + ')'