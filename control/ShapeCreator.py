from shape.Shape2D import Shape2D
from shape.Triangle2D import Triangle2D
from shape.Line2D import Line2D
from shape.Point2D import Point2D
from control.ShapeJson import ShapeJson
from random import randint

class ShapeCreator:
    
    def __init__(self):
        self._shape = Shape2D()
    
    def __generatePoints2D(self, n):
        pointlist = []

        num = n
        while num > 0:
            x = randint(1,50)
            y = randint(1,50)
            num = num - 1
            pointlist.append(Point2D(x,y))
        
        return pointlist
    
    def __getAmountOfShapes(self):
        amount = input("Wieviele Figuren ?\n")
        try:
            int(amount)
            if int(amount) >= 1 and int(amount) <= 16:
                return int(amount)
        except Exception:
            print("Bitte nur eine Zahl eingeben")
            self._getAmountOfShapes()
        else:
            print('Anzahl der Figuren [1,16]')
            return 0
        finally:
            pass
    
    def createShape(self):
        shapetype = input("Figur eingeben ([l]ine, [t]riangle, [d]rucken, [s]peichern, [r]lesen) :\n")
        if shapetype == 'l':
            amount = self._getAmountOfShapes()
            points = self._generatePoints2D(2 * amount)
            for i in range(0, len(points), 2):
                self._shape.addShape(Line2D.createLine2DFromPoint2D(points[i], points[i+1]))
                
            self.createShape()
                
        elif shapetype == 't':
            points = self._generatePoints2D(3 * self._getAmountOfShapes())
            for i in range(0, len(points), 3):
                self._shape.addShape(Triangle2D.createTriangle2DFromPoint2D(points[i], points[i+1], points[i+2]))

            self.createShape()
                
        elif shapetype == 'd':
            for i in range(len(self._shape._shapelist)):
                print(repr(self._shape.getShapelist()[i]))
                print(hash(self._shape.getShapelist()[i]))
                
        elif shapetype == 's':
            ShapeJson(self,shapetype)
        elif shapetype == 'r':
            ShapeJson(self,shapetype)
        else:           
            print('Unbekannte Figur')
            
    def setInfo(self, info):
        print(info)
        
    def getShapeInstance(self):
        return self._shape
            
sc = ShapeCreator()
sc.createShape()

        
        