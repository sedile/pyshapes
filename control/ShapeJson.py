import json

class ShapeJson:
    
    def __init__(self, sc, event):
        self._sc = sc
        if (event == 's' ):
            data = self.writeData()
            self.saveFile(data)
        elif (event == 'r' ):
            data = self.loadFile()
            self.readData(data)
            
    def writeData(self):
        shapelist = self._sc.getShapeInstance().getShapelist()
        data = {}
        data['shapes'] = []
        
        for i in range(len(shapelist)):
            if shapelist[i].getShapeType() == 'Triangle2D':
                data['shapes'].append({'type':shapelist[i].getShapeType(),
                               'points':[shapelist[i].getLowerLeft().toString(),
                                         shapelist[i].getLowerRight().toString(),
                                         shapelist[i].getTop().toString()],
                               'shaperepr':shapelist[i].toString()
                               })
            elif shapelist[i].getShapeType() == 'Line2D':
                data['shapes'].append({'type':shapelist[i].getShapeType(),
                                      'points':[shapelist[i].getStartPoint().toString(),
                                                shapelist[i].getEndPoint().toString()],
                                      'shaperepr':shapelist[i].toString()
                                })
            else:
                break
            
        return data
        
    def saveFile(self, data):
        with open('/home/sebastian/Dokumente/pythonCode/Shapes/shapetest.json','w') as output:
            json.dump(data, output)
        
    def loadFile(self):
        with open('/home/sebastian/Dokumente/pythonCode/Shapes/shapetest.json','r') as iput:
            data = json.load(iput)
            return data
        
    def readData(self, data):
        for i in data['shapes']:
            print("Figurentype : " + str(i['type']))
            print("Punkteliste : " + str(i['points']))
            print("Darstellung : " + str(i['shaperepr']))
            print()
        
        
